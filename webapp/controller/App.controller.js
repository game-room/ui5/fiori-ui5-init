sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function(BaseController) {
      "use strict";
  
      return BaseController.extend("jega.invoiceslist.controller.App", {
        onInit: function() {
        }
      });
    }
  );
  