/* global QUnit */

sap.ui.require(["jega/invoiceslist/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
