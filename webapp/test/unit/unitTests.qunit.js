/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"jega/invoices_list/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
